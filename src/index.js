import Phaser from "phaser";
import PhaserMatterCollisionPlugin from "phaser-matter-collision-plugin";
//main
import main_window from "./assets/Main_menu/main01.jpg";
import ps_main from "./assets/Main_menu/main-ps.png";
import out_main from "./assets/Main_menu/main-out.png";
import l200_main from "./assets/Main_menu/main-l200.png";
//L200
import L200 from "./assets/L200_level/L200_model/L200.png";
import l200_bg from "./assets/L200_level/l200-BG-big.jpg";
import l200_shadow from "./assets/L200_level/L200_model/L200-shadow.png";
//Outlander
import Outlander from "./assets/Outlander_level/Out_model/OUT.png";
import out_bg from "./assets/Outlander_level/Out-BG-big.jpg";
import out_shadow from "./assets/Outlander_level/Out_model/OUT-shadow.png";
//Pajero Sport
import PajeroSport from "./assets/PS_level/PS_model/PS.png";
import ps_bg from "./assets/PS_level/PS-BG-big.jpg";
import ps_shadow from "./assets/PS_level/PS_model/PS-shadow.png";

//other
import wheel from "./assets/wheel.png";
import rats from "./assets/Main_menu/Rats-main.png";

//music
import menu_fx from "./assets/Sounds/final_s.mp3";
import l200_fx from "./assets/Sounds/l3.mp3";
import out_fx from "./assets/Sounds/l2.mp3";
import ps_fx from "./assets/Sounds/l1.mp3";

//Out Toys
import toy_1 from "./assets/Outlander_level/Out_toys/1.png";
import toy_2 from "./assets/Outlander_level/Out_toys/2.png";
import toy_3 from "./assets/Outlander_level/Out_toys/3.png";
import toy_4 from "./assets/Outlander_level/Out_toys/4.png";
import toy_5 from "./assets/Outlander_level/Out_toys/5.png";
import toy_6 from "./assets/Outlander_level/Out_toys/6.png";
import toy_7 from "./assets/Outlander_level/Out_toys/7.png";
import toy_8 from "./assets/Outlander_level/Out_toys/8.png";
import toy_9 from "./assets/Outlander_level/Out_toys/9.png";
import toy_10 from "./assets/Outlander_level/Out_toys/10.png";
import toy_11 from "./assets/Outlander_level/Out_toys/11.png";
import toy_12 from "./assets/Outlander_level/Out_toys/12.png";
import toy_13 from "./assets/Outlander_level/Out_toys/13.png";
import toy_14 from "./assets/Outlander_level/Out_toys/14.png";
import toy_15 from "./assets/Outlander_level/Out_toys/15.png";
import toy_16 from "./assets/Outlander_level/Out_toys/16.png";
import toy_17 from "./assets/Outlander_level/Out_toys/17.png";
import toy_18 from "./assets/Outlander_level/Out_toys/18.png";
import toy_19 from "./assets/Outlander_level/Out_toys/19.png";
import toy_20 from "./assets/Outlander_level/Out_toys/20.png";
import toy_21 from "./assets/Outlander_level/Out_toys/21.png";
//Out Rats
import mouse_png from "./assets/Outlander_level/Out_mouse/mouse_waiting.png";
import mouse_json from "./assets/Outlander_level/Out_mouse/mouse_waiting.json";
import mouse_model from "./assets/Outlander_level/Out_mouse/m_1.png";
import mouse_run_png from "./assets/Outlander_level/Out_mouse_running/mouse_running.png";
import mouse_run_json from "./assets/Outlander_level/Out_mouse_running/mouse_running.json";
import mouse_run_model from "./assets/Outlander_level/Out_mouse_running/m_1.png";

//L200 snow
import snow_1 from "./assets/L200_level/L200_snow/01.png";
import snow_2 from "./assets/L200_level/L200_snow/02.png";
import snow_3 from "./assets/L200_level/L200_snow/03.png";
import snow_4 from "./assets/L200_level/L200_snow/04.png";
import snow_5 from "./assets/L200_level/L200_snow/05.png";
import snow_6 from "./assets/L200_level/L200_snow/06.png";
import snow_7 from "./assets/L200_level/L200_snow/07.png";
import snow_8 from "./assets/L200_level/L200_snow/08.png";
import snow_9 from "./assets/L200_level/L200_snow/09.png";
//L200 prizes
import prize_1 from "./assets/L200_level/L200_prizes/01.png";
import prize_2 from "./assets/L200_level/L200_prizes/02.png";
import prize_3 from "./assets/L200_level/L200_prizes/03.png";
import prize_4 from "./assets/L200_level/L200_prizes/04.png";
import prize_5 from "./assets/L200_level/L200_prizes/05.png";
import prize_6 from "./assets/L200_level/L200_prizes/06.png";
import prize_7 from "./assets/L200_level/L200_prizes/07.png";
import prize_8 from "./assets/L200_level/L200_prizes/08.png";
import prize_9 from "./assets/L200_level/L200_prizes/09.png";
import prize_10 from "./assets/L200_level/L200_prizes/10.png";
import prize_11 from "./assets/L200_level/L200_prizes/11.png";
import prize_12 from "./assets/L200_level/L200_prizes/12.png";

//PS Dishes
import dish_1 from "./assets/PS_level/PS_dishes/01.png";
import dish_2 from "./assets/PS_level/PS_dishes/02.png";
import dish_3 from "./assets/PS_level/PS_dishes/03.png";
import dish_4 from "./assets/PS_level/PS_dishes/04.png";

//PS Food
import food_1 from "./assets/PS_level/PS_food/01.png";
import food_2 from "./assets/PS_level/PS_food/02.png";
import food_3 from "./assets/PS_level/PS_food/03.png";
import food_4 from "./assets/PS_level/PS_food/04.png";
import food_5 from "./assets/PS_level/PS_food/05.png";
import food_6 from "./assets/PS_level/PS_food/06.png";
import food_7 from "./assets/PS_level/PS_food/07.png";

//BOOM
import boom_png from "./assets/Boom_anim/boom.png";
import boom_json from "./assets/Boom_anim/boom.json";

//Final
import face_main from "./assets/Final/room_merry.png";
import tree from "./assets/Final/tree.png";
import rate from "./assets/Final/rate.png";
import food from "./assets/Final/food.png";

//Control
import control from "./assets/Controls/controll-manual.png";
import acceleration from "./assets/Controls/acceleration.png";
import brake from "./assets/Controls/brake.png";
import reverse from "./assets/Controls/reverse.png";
import m_left from "./assets/Controls/left.png";
import m_right from "./assets/Controls/right.png";
import grey from "./assets/grey.png";
import audio from "./assets/audio.png";

//socials
import vk from "./assets/Socials/vk.png";
import inst from "./assets/Socials/inst.png";
import ok from "./assets/Socials/ok.png";
import fb from "./assets/Socials/fb.png";

//vars
let model;
let cursors;
let car_shadow;
let currentSpeed = 0;
let bg;
let camera;
let time;
let l_wheel;
let r_wheel;
let intro;
let score;
let bump;
let gameTimer;
var l200_done = false;
var out_done = false;
var ps_done = false;
var notice = 0;
var l200_score = 0;
var l200_bump = 0;
var out_score = 0;
var out_bump = 0;
var ps_score = 0;
var ps_bump = 0;
var l200_total = 0;
var out_total = 0;
var ps_total = 0;
var game_total = 0;
var snapHistory = [];
var forward = false;
var backward = false;
var brakes = false;
var left = false;
var right = false;
var boot_type;

var textStyle = {
    font: '32px MMC OFFICE', fill: 'white'
};
var textCon = {
    font: '28px MMC OFFICEBOLD', fill: 'white',
};
var annoStyle = {
    font: '36px MMC OFFICEBOLD', fill: 'red',
};

var totalStyle = {
    font: '54px MMC OFFICEBOLD', fill: 'red',
};

var manStyle = {
    font: '36px MMC OFFICE', fill: 'white'
};

var mainStyle = {
    font: '24px MMC OFFICE', fill: 'white'
};

//Final loader
let FinalScene = Phaser.Class({
    Extends: Phaser.Scene,
    initialize:
        function FinalScene() {
            Phaser.Scene.call(this, {key: 'finalScene'});
        },
    preload: function () {
        this.load.image("face-main", face_main);
        this.load.image("tree", tree);
        this.load.image("rate", rate);
        this.load.image("food", food);
        this.load.image('over', grey);
        this.load.image('vk', vk);
        this.load.image('ok', ok);
        this.load.image('inst', inst);
        this.load.image('fb', fb);
    },
    create: function () {
        this.face = this.add.image(960, 515, 'face-main');
        this.initialTime = 12;
        let note = this.add.text(600,400,'',annoStyle);
        let text_back = this.add.text(880,1280,'',textCon);
        let overlay = this.add.image(960, 515, 'over');
        overlay.setDepth(5).setVisible(false).setAlpha(0.3, 0.3);
        text_back.setScrollFactor(0).setInteractive().on('pointerdown', function () {
            this.scene.start('loaderScene');
        });

        this.cameras.main.fadeIn(1500);
        this.cameras.main.on('camerafadeincomplete', function () {
            overlay.setVisible(true);
            if (notice === 1) {
                note.setText('Отлично, вечер перестаёт быть томным!').setDepth(6);
                text_back.setText('Продолжить').setDepth(6);
            }
            if (notice === 2) {
                note.setText('Осталось совсем немного и можно начинать вечеринку!').setDepth(6);
                text_back.setText('Продолжить').setDepth(6);
            }
            if (notice === 3) {
                note.setText('Ура! Новому году быть! ' + 'Спасибо за старания!').setDepth(6);
            }
        });
        let currentScore = this.add.text(600, 600, '', textCon);
        currentScore.setDepth(40);
        let currentScore2 = this.add.text(600, 700, '', textCon);
        currentScore2.setDepth(40);
        let currentScore3 = this.add.text(600, 800, '', textCon);
        currentScore3.setDepth(40);
        if (l200_done === true) {
            let tree = this.add.image(960, 515, 'tree').setDepth(2);
            countScore(currentScore, 'l200');
        }
        if (out_done === true) {
            let rate = this.add.image(960, 515, 'rate').setDepth(2);
            countScore(currentScore2, 'outlander');
        }
        if (ps_done === true) {
            let food = this.add.image(960, 515, 'food').setDepth(2);
            countScore(currentScore3, 'ps');
        }

        notice += 1;

        if (notice < 3) {
            gameTimer = this.time.addEvent({
                delay: 1000,
                callback: onBootLoad,
                startAt: 3000,
                callbackScope: this,
                loop: true
            });
        } else {
            let total_score = this.add.text(600, 500, '', totalStyle).setDepth(50);
            game_total = total(l200_total, l200_score, l200_bump) + total(out_total, out_score, out_bump) + total(ps_total, ps_score, ps_bump);
            total_score.setText('Всего очков: ' + game_total);
            let vk = this.add.image(700, 904, "vk").setDepth(100).setScale(0.5);
            let ok = this.add.image(900, 900, "ok").setDepth(100).setScale(0.4);
            let fb = this.add.image(1100, 900, "fb").setDepth(100).setScale(0.4);
            let inst = this.add.image(1300, 900, "inst").setDepth(100).setScale(0.4);
            let again = this.add.text(840,1000,'',textCon).setDepth(111);
            again.setInteractive().on('pointerdown', function () {
                l200_done = false;
                out_done = false;
                ps_done = false;
                l200_bump = 0;
                l200_score = 0;
                out_bump = 0;
                out_score = 0;
                ps_score = 0;
                ps_bump = 0;
                l200_total = 0;
                out_total = 0;
                ps_total = 0;
                game_total = 0;
                this.scene.scene.start("loaderScene");
            });
            again.setText('Попробовать еще раз');
            // game.renderer.snapshot(function (image) {
            //     document.body.appendChild(image);
            //     // document.write(VK.Share.button({url: 'https://preprod.mitsubishilink.ru', title: 'Midh'}, {type: 'custom', text: image}));
            // });
        }
    },
});

//Boot loader
let BootScene = Phaser.Class({
    Extends: Phaser.Scene,
    initialize:
        function BootScene() {
            Phaser.Scene.call(this, {key: 'bootScene'});
        },
    preload: function () {
        this.load.image("control", control);
    },
    create: function () {
        this.initialTime = 6;
        let man = this.add.image(960, 515, 'control');
        man.setScale(0.7, 0.7);
        let info = this.add.text(440, 100, '', mainStyle);
        let info2 = this.add.text(780, 150, '', mainStyle);
        if (boot_type === 0) {
            bootOrder('l1Scene', info, info2);
            gameTimer = this.time.addEvent({
                delay: 1000,
                callback: onBootL200,
                startAt: 3000,
                callbackScope: this,
                loop: true
            });
        }
        if (boot_type === 1) {
            bootOrder('l2Scene', info, info2);
            gameTimer = this.time.addEvent({
                delay: 1000,
                callback: onBootPS,
                startAt: 3000,
                callbackScope: this,
                loop: true
            });
        }
        if (boot_type === 2) {
            bootOrder('l3Scene', info, info2);
            gameTimer = this.time.addEvent({
                delay: 1000,
                callback: onBootOut,
                startAt: 3000,
                callbackScope: this,
                loop: true
            });
        }
    },
});

//Menu loader
let LoaderScene = Phaser.Class({
    Extends: Phaser.Scene,
    initialize:
        function LoaderScene() {
            Phaser.Scene.call(this, {key: 'loaderScene'});
        },
    preload: function () {
        this.load.image("main", main_window);
        this.load.image("main-l200", l200_main);
        this.load.image("main-out", out_main);
        this.load.image("main-ps", ps_main);
        this.load.image("main-rats", rats);
        this.load.audio("menu", menu_fx);
        this.load.image("audio", audio);
    },
    create: function () {
        this.cameras.main.fadeIn(1500);
        this.face = this.add.image(960, 515, 'main');
        let ml200 = this.add.image(330, 710, "main-l200");
        let mps = this.add.image(670, 710, "main-ps");
        let mout = this.add.image(1065, 710, "main-out");
        let rats_png = this.add.image(900, 500, "main-rats");
        rats_png.setDepth(10).setVisible(false);
        let playSound = this.add.image(1850, 50, 'audio');
        playSound.setScrollFactor(0).setDepth(200).setScale(0.1);
        audioControl(playSound);
        let loader_m = this.sound.add("menu");
        let start_text = this.add.text(520, 150, 'Новый год не за горами!\n' +
            '\n' +
            'А семья крыс живущая за дровником у дачного забора совсем не готова к встрече праздника.\n' +
            'А ведь они символ наступающего года!\n' +
            '\n' +
            'Помоги им собраться вместе и встретить Новый год.\n' +
            'Выбирай машину, и вперёд!', mainStyle).setVisible(false);
        this.cameras.main.on('camerafadeincomplete', function () {
            rats_png.setVisible(true);
            start_text.setVisible(true);
            loader_m.play({loop: false});
        });
        if (l200_done === false) {
            ml200.setInteractive().on('pointerdown', function () {
                boot_type = 0;
                this.scene.scene.start('bootScene');
            });
            ml200.setInteractive().on('pointerover', function () {
                ml200.setDisplaySize(513 * 1.15, 237 * 1.15);
            });
            ml200.setInteractive().on('pointerout', function () {
                ml200.setDisplaySize(513, 237);
            });
        }
        if (out_done === false) {
            mout.setInteractive().on('pointerdown', function () {
                boot_type = 1;
                this.scene.scene.start('bootScene');
            });
            mout.setInteractive().on('pointerover', function () {
                mout.setDisplaySize(466 * 1.15, 237 * 1.15);
            });
            mout.setInteractive().on('pointerout', function () {
                mout.setDisplaySize(466, 237);
            });
        }
        if (ps_done === false) {
            mps.setInteractive().on('pointerdown', function () {
                boot_type = 2;
                this.scene.scene.start('bootScene');
            });
            mps.setInteractive().on('pointerover', function () {
                mps.setDisplaySize(538 * 1.15, 248 * 1.15);
            });
            mps.setInteractive().on('pointerout', function () {
                mps.setDisplaySize(538, 248);
            });
        }
    }
});

let L200Scene = Phaser.Class({
    Extends: Phaser.Scene,
    initialize:
        function L1Scene() {
            Phaser.Scene.call(this, {key: 'l1Scene'});
        },
    preload: function () {
        this.load.image("l200", L200);
        this.load.image("l200bg", l200_bg);
        this.load.image("wheels", wheel);
        this.load.image("shadow_img", l200_shadow);
        this.load.audio("l200_music", l200_fx);
        this.load.image("snow_1", snow_1);
        this.load.image("snow_2", snow_2);
        this.load.image("snow_3", snow_3);
        this.load.image("snow_4", snow_4);
        this.load.image("snow_5", snow_5);
        this.load.image("snow_6", snow_6);
        this.load.image("snow_7", snow_7);
        this.load.image("snow_8", snow_8);
        this.load.image("snow_9", snow_9);

        this.load.image("prize_1", prize_1);
        this.load.image("prize_2", prize_2);
        this.load.image("prize_3", prize_3);
        this.load.image("prize_4", prize_4);
        this.load.image("prize_5", prize_5);
        this.load.image("prize_6", prize_6);
        this.load.image("prize_7", prize_7);
        this.load.image("prize_8", prize_8);
        this.load.image("prize_9", prize_9);
        this.load.image("prize_10", prize_10);
        this.load.image("prize_11", prize_11);
        this.load.image("prize_12", prize_12);

        this.load.atlas("boom", boom_png, boom_json);

        this.load.image("acc", acceleration);
        this.load.image("rvr", reverse);
        this.load.image("brk", brake);
        this.load.image("ml", m_left);
        this.load.image("mr", m_right);
        this.load.image("audio", audio);
    },
    create: function () {
        let playSound = this.add.image(1850, 50, 'audio').setScrollFactor(0).setDepth(200).setScale(0.1);
        let mute = false;
        playSound.setInteractive().on('pointerdown', function () {
            if (mute === false) {
                this.scene.sound.setVolume(0);
                mute = true;
            } else {
                this.scene.sound.setVolume(1);
                mute = false;
            }
        });
        let snow_1 = this.matter.add.image(220, 1400, 'snow_1', null, {isStatic: true});
        let snow_2 = this.matter.add.image(1520, 50, 'snow_2', null, {isStatic: true});
        let snow_3 = this.matter.add.image(2400, 600, 'snow_3', null, {isStatic: true});
        let snow_4 = this.matter.add.image(400, 800, 'snow_4', null, {isStatic: true});
        let snow_5 = this.matter.add.image(1020, 3100, 'snow_5', null, {isStatic: true});
        let snow_6 = this.matter.add.image(1700, 1900, 'snow_6', null, {isStatic: true});
        let snow_7 = this.matter.add.image(2420, 2400, 'snow_7', null, {isStatic: true});
        let snow_8 = this.matter.add.image(720, 2400, 'snow_8', null, {isStatic: true});
        let snow_9 = this.matter.add.image(1800, 1200, 'snow_9', null, {isStatic: true});
        snow_1.setDepth(1).setScale(0.7);
        snow_2.setDepth(1).setScale(0.7);
        snow_3.setDepth(1).setScale(0.7);
        snow_4.setDepth(1).setScale(0.7);
        snow_5.setDepth(1).setScale(0.7);
        snow_6.setDepth(1).setScale(0.7);
        snow_7.setDepth(1).setScale(0.7);
        snow_8.setDepth(1).setScale(0.7);
        snow_9.setDepth(1).setScale(0.7);
        let prize_1 = this.matter.add.sprite(920, 300, 'prize_1', null, {isStatic: true});
        prize_1.setScale(0.5, 0.5);
        let prize_2 = this.matter.add.image(2320, 150, 'prize_2', null, {isStatic: true});
        prize_2.setScale(0.5, 0.5);
        let prize_3 = this.matter.add.image(2350, 850, 'prize_3', null, {isStatic: true});
        prize_3.setScale(0.5, 0.5);
        let prize_4 = this.matter.add.image(610, 1100, 'prize_4', null, {isStatic: true});
        prize_4.setScale(0.5, 0.5);
        let prize_5 = this.matter.add.image(100, 1050, 'prize_5', null, {isStatic: true});
        prize_5.setScale(0.5, 0.5);
        let prize_6 = this.matter.add.image(1820, 1400, 'prize_6', null, {isStatic: true});
        prize_6.setScale(0.5, 0.5);
        let prize_7 = this.matter.add.image(820, 1700, 'prize_7', null, {isStatic: true});
        prize_7.setScale(0.5, 0.5);
        let prize_8 = this.matter.add.image(2350, 1850, 'prize_8', null, {isStatic: true});
        prize_8.setScale(0.5, 0.5);
        let prize_9 = this.matter.add.image(2420, 2300, 'prize_9', null, {isStatic: true});
        prize_9.setScale(0.5, 0.5);
        let prize_10 = this.matter.add.image(2320, 2750, 'prize_10', null, {isStatic: true});
        prize_10.setScale(0.5, 0.5);
        let prize_11 = this.matter.add.image(1200, 2700, 'prize_11', null, {isStatic: true});
        prize_11.setScale(0.5, 0.5);
        let prize_12 = this.matter.add.image(220, 2800, 'prize_12', null, {isStatic: true});
        prize_12.setScale(0.5, 0.5);
        prize_1.setDepth(5);
        prize_2.setDepth(5);
        prize_3.setDepth(5);
        prize_4.setDepth(5);
        prize_5.setDepth(5);
        prize_6.setDepth(5);
        prize_7.setDepth(5);
        prize_8.setDepth(5);
        prize_9.setDepth(5);
        prize_10.setDepth(5);
        prize_11.setDepth(5);
        prize_12.setDepth(5);

        var l200_m = this.sound.add("l200_music");
        l200_m.play({loop: true});
        let eld = document.createElement('div');
        this.initialTime = 60;
        camera = this.cameras.main;
        camera.setBounds(0, 0, 2500, 3000);
        this.matter.world.setBounds(0, 0, 2500, 3000).disableGravity();
        bg = this.add.image(1250, 1500, "l200bg");
        bg.setDepth(0);
        bg.setAngle(90);
        if (!this.sys.game.device.os.desktop) {
            let ac = this.add.image(1800, 800, "acc");
            let rv = this.add.image(1500, 850, "rvr");
            let br = this.add.image(960, 850, "brk");
            let mr = this.add.image(500, 850, "mr");
            let ml = this.add.image(200, 850, "ml");
            ac.setDepth(100);
            rv.setDepth(100);
            br.setDepth(100);
            mr.setDepth(100);
            ml.setDepth(100);
            ac.setScrollFactor(0);
            rv.setScrollFactor(0);
            br.setScrollFactor(0);
            mr.setScrollFactor(0);
            ml.setScrollFactor(0);

            ac.setInteractive().on('pointerdown', function () {
                forward = true;
                backward = false;
            });

            ac.setInteractive().on('pointerout', function () {
                forward = false;
                backward = false;
            });

            rv.setInteractive().on('pointerdown', function () {
                backward = true;
                forward = false;
            });
            rv.setInteractive().on('pointerout', function () {
                backward = false;
                forward = false;
            });

            br.setInteractive().on('pointerdown', function () {
                brakes = true;
            });
            br.setInteractive().on('pointerout', function () {
                brakes = false;
            });

            ml.setInteractive().on('pointerdown', function () {
                left = true;
                right = false;
            });
            ml.setInteractive().on('pointerout', function () {
                left = false;
                right = false;
            });

            mr.setInteractive().on('pointerdown', function () {
                right = true;
                left = false;
            });

            mr.setInteractive().on('pointerout', function () {
                right = false;
                left = false;
            });

            forward = false;
            backward = false;
            brakes = false;
            left = false;
            right = false;

        }

        model = this.matter.add.sprite(500, 500, "l200");
        car_shadow = this.matter.add.image(1000, 1000, "shadow_img");
        car_shadow.body.destroy();
        l_wheel = this.matter.add.image(0, 0, "wheels");
        r_wheel = this.matter.add.image(0, 0, "wheels");
        l_wheel.displayHeight = 36;
        r_wheel.displayHeight = 36;
        r_wheel.displayWidth = 14;
        l_wheel.displayWidth = 14;

        model.setBody({
            type: 'rectangle',
            height: '280',
            width: '90'
        });

        model.setDepth(5);
        l_wheel.setDepth(4);
        r_wheel.setDepth(4);
        car_shadow.setDepth(2);
        car_shadow.setAlpha(0.5);
        l_wheel.body.destroy();
        r_wheel.body.destroy();

        camera.startFollow(model, true, 0.1, 0.1);
        cursors = this.input.keyboard.createCursorKeys();
        time = this.add.text(20, 100, '', textStyle);
        time.setScrollFactor(0);
        time.setDepth(30);
        gameTimer = this.time.addEvent({
            delay: 1000,
            callback: onEvent,
            startAt: 3000,
            callbackScope: this,
            loop: true
        });
        let i = 0;
        this.matterCollision.addOnCollideStart({
            objectA: model,
            objectB: [prize_1, prize_2, prize_3, prize_4, prize_5, prize_6, prize_7, prize_8, prize_9, prize_10, prize_11, prize_12],
            callback: eventData => {
                eventData.gameObjectB.destroy();
                l200_score += 10;
            }
        });

        this.matterCollision.addOnCollideStart({
            objectA: model,
            objectB: [snow_1, snow_2, snow_3, snow_4, snow_5, snow_6, snow_7, snow_8, snow_9],
            callback: eventData => {
                l200_bump += 1;
            }
        });

        score = this.add.text(20, 20, '', textStyle);
        score.setScrollFactor(0);
        score.setDepth(20);
        bump = this.add.text(20, 60, '', textStyle);
        bump.setScrollFactor(0);
        bump.setDepth(20);
    },
    update: function () {

        if (this.initialTime === 1) {
            l200_done = true;
            this.sound.stopAll();
            car_shadow.destroy();
        } else if (l200_score === 120) {
            l200_done = true;
            forward = false;
            backward = false;
            brakes = false;
            left = false;
            right = false;
            currentSpeed = 0;
            this.sound.stopAll();
            this.scene.start('finalScene');
        }

        if (cursors.up.isDown && currentSpeed <= 10 || forward && currentSpeed <= 10) {
            currentSpeed += 0.5;
        } else {
            if (currentSpeed > 0) {
                currentSpeed -= 0.5;
            }
        }

        if (cursors.down.isDown && currentSpeed >= -5 || backward && currentSpeed >= -5) {
            currentSpeed -= 0.5;
        } else {
            if (currentSpeed < 0) {
                currentSpeed += 0.5;
            }
        }

        if (cursors.space.isDown || brakes) {
            model.setVelocityX(currentSpeed * (Math.sin(model.rotation - model.body.angularVelocity / 0.05)));
            model.setVelocityY(currentSpeed * (-Math.cos(model.rotation - model.body.angularVelocity / 0.05)));
        } else {
            model.setVelocityX(Math.sin(model.rotation) * currentSpeed);
            model.setVelocityY(-Math.cos(model.rotation) * currentSpeed);
        }

        let leftWheelPos = {x: model.x - 45, y: model.y - 100};
        let rightWheelPos = {x: model.x + 45, y: model.y - 100};
        Phaser.Math.RotateAround(leftWheelPos, model.x, model.y, model.rotation);
        l_wheel.setPosition(leftWheelPos.x, leftWheelPos.y);
        Phaser.Math.RotateAround(rightWheelPos, model.x, model.y, model.rotation);
        r_wheel.setPosition(rightWheelPos.x, rightWheelPos.y);

        if (cursors.left.isDown || left) {
            model.setAngularVelocity(-5 * (currentSpeed / 1000));
            l_wheel.angle = model.angle - 35;
            r_wheel.angle = model.angle - 35;
        } else if (cursors.right.isDown || right) {
            model.setAngularVelocity(5 * (currentSpeed / 1000));
            l_wheel.angle = model.angle + 35;
            r_wheel.angle = model.angle + 35;
        } else {
            setTimeout(function () {
                model.setAngularVelocity(0);
            }, 100);
            l_wheel.angle = model.angle;
            r_wheel.angle = model.angle;
        }

        car_shadow.x = model.x;
        car_shadow.y = model.y;
        car_shadow.setRotation(model.rotation);

        score.setText('Очки за подарки: ' + l200_score);
        bump.setText('Штраф за столкновения: ' + l200_bump);
    }
});

let OutlanderScene = Phaser.Class({
    Extends: Phaser.Scene,
    initialize:
        function L2Scene() {
            Phaser.Scene.call(this, {key: 'l2Scene'});
        },

    preload: function () {
        this.load.image("outlander", Outlander);
        this.load.image("outbg", out_bg);
        this.load.image("wheels", wheel);
        this.load.image("shadow_img", out_shadow);
        this.load.image("toy_1", toy_1);
        this.load.image("toy_2", toy_2);
        this.load.image("toy_3", toy_3);
        this.load.image("toy_4", toy_4);
        this.load.image("toy_5", toy_5);
        this.load.image("toy_6", toy_6);
        this.load.image("toy_7", toy_7);
        this.load.image("toy_8", toy_8);
        this.load.image("toy_9", toy_9);
        this.load.image("toy_10", toy_10);
        this.load.image("toy_11", toy_11);
        this.load.image("toy_12", toy_12);
        this.load.image("toy_13", toy_13);
        this.load.image("toy_14", toy_14);
        this.load.image("toy_15", toy_15);
        this.load.image("toy_16", toy_16);
        this.load.image("toy_17", toy_17);
        this.load.image("toy_18", toy_18);
        this.load.image("toy_19", toy_19);
        this.load.image("toy_20", toy_20);
        this.load.image("toy_21", toy_21);

        this.load.atlas("mouse", mouse_png, mouse_json);
        this.load.image("mouse_m", mouse_model);

        this.load.atlas("mouse_run", mouse_run_png, mouse_run_json);
        this.load.image("mouse_r", mouse_run_model);

        this.load.audio("out_music", out_fx);

        this.load.image("acc", acceleration);
        this.load.image("rvr", reverse);
        this.load.image("brk", brake);
        this.load.image("ml", m_left);
        this.load.image("mr", m_right);
        this.load.image("audio", audio);
    },
    create: function () {
        let playSound = this.add.image(1850, 50, 'audio').setScrollFactor(0).setDepth(200).setScale(0.1);
        let mute = false;
        playSound.setInteractive().on('pointerdown', function () {
            if (mute === false) {
                this.scene.sound.setVolume(0);
                mute = true;
            } else {
                this.scene.sound.setVolume(1);
                mute = false;
            }
        });
        var out_m = this.sound.add("out_music");
        out_m.play({loop: true});
        let toy_1 = this.matter.add.image(100, 300, 'toy_1', null, {isStatic: true}).setBounce(1).setFriction(0);
        let toy_2 = this.matter.add.image(220, 400, 'toy_2', null, {isStatic: true}).setBounce(0);
        let toy_3 = this.matter.add.image(420, 600, 'toy_3', null, {isStatic: true});
        let toy_4 = this.matter.add.image(600, 700, 'toy_4', null, {isStatic: true});
        let toy_5 = this.matter.add.image(760, 450, 'toy_5', null, {isStatic: true});
        let toy_6 = this.matter.add.image(800, 800, 'toy_6', null, {isStatic: true});
        let toy_7 = this.matter.add.image(720, 1000, 'toy_7', null, {isStatic: true});
        let toy_8 = this.matter.add.image(1320, 1600, 'toy_8', null, {isStatic: true});
        let toy_9 = this.matter.add.image(1420, 1200, 'toy_9', null, {isStatic: true});
        let toy_10 = this.matter.add.image(1120, 2200, 'toy_10', null, {isStatic: true});
        let toy_11 = this.matter.add.image(1720, 1600, 'toy_11', null, {isStatic: true});
        let toy_12 = this.matter.add.image(2120, 2600, 'toy_12', null, {isStatic: true});
        let toy_13 = this.matter.add.image(1320, 2600, 'toy_13', null, {isStatic: true});
        let toy_14 = this.matter.add.image(2320, 1600, 'toy_14', null, {isStatic: true});
        let toy_15 = this.matter.add.image(2350, 2700, 'toy_15', null, {isStatic: true});
        let toy_16 = this.matter.add.image(2320, 900, 'toy_16', null, {isStatic: true});
        let toy_17 = this.matter.add.image(1101, 1100, 'toy_17', null, {isStatic: true});
        let toy_18 = this.matter.add.image(750, 2200, 'toy_18', null, {isStatic: true});
        let toy_19 = this.matter.add.image(1320, 2600, 'toy_19', null, {isStatic: true});
        let toy_20 = this.matter.add.image(1110, 300, 'toy_20', null, {isStatic: true});
        let toy_21 = this.matter.add.image(320, 1600, 'toy_21', null, {isStatic: true});

        let walk = {
            key: 'walk',
            frames: [
                {key: "mouse", frame: "m_1.png"},
                {key: "mouse", frame: "m_2.png"},
                {key: "mouse", frame: "m_3.png"},
                {key: "mouse", frame: "m_4.png"},
                {key: "mouse", frame: "m_5.png"},
                {key: "mouse", frame: "m_6.png"},
                {key: "mouse", frame: "m_7.png"},
                {key: "mouse", frame: "m_8.png"},
                {key: "mouse", frame: "m_9.png"},
                {key: "mouse", frame: "m_10.png"},
                {key: "mouse", frame: "m_11.png"},
                {key: "mouse", frame: "m_12.png"},
                {key: "mouse", frame: "m_13.png"},
                {key: "mouse", frame: "m_14.png"},
                {key: "mouse", frame: "m_15.png"},
            ],
            frameRate: 7,
            repeat: -1
        };

        this.anims.create(walk);

        let mouse_mod = this.matter.add.sprite(1000, 1000, 'mouse_m', null, {isStatic: true});
        mouse_mod.anims.load('walk');
        mouse_mod.anims.play('walk');
        mouse_mod.setDepth(11);
        mouse_mod.setScale(0.5, 0.5);
        let mouse_mod2 = this.matter.add.sprite(2000, 2000, 'mouse_m', null, {isStatic: true});
        mouse_mod2.anims.load('walk');
        mouse_mod2.anims.play('walk');
        mouse_mod2.setDepth(11);
        mouse_mod2.setScale(0.5, 0.5);
        let mouse_mod3 = this.matter.add.sprite(500, 1500, 'mouse_m', null, {isStatic: true});
        mouse_mod3.anims.load('walk');
        mouse_mod3.anims.play('walk');
        mouse_mod3.setDepth(11);
        mouse_mod3.setScale(0.5, 0.5);
        let mouse_mod4 = this.matter.add.sprite(1800, 500, 'mouse_m', null, {isStatic: true});
        mouse_mod4.anims.load('walk');
        mouse_mod4.anims.play('walk');
        mouse_mod4.setDepth(11);
        mouse_mod4.setScale(0.5, 0.5);
        let mouse_mod5 = this.matter.add.sprite(2200, 2800, 'mouse_m', null, {isStatic: true});
        mouse_mod5.anims.load('walk');
        mouse_mod5.anims.play('walk');
        mouse_mod5.setDepth(11);
        mouse_mod5.setScale(0.5, 0.5);
        let mouse_mod6 = this.matter.add.sprite(400, 2000, 'mouse_m', null, {isStatic: true});
        mouse_mod6.anims.load('walk');
        mouse_mod6.anims.play('walk');
        mouse_mod6.setDepth(11);
        mouse_mod6.setScale(0.5, 0.5);
        let mouse_mod7 = this.matter.add.sprite(1100, 2500, 'mouse_m', null, {isStatic: true});
        mouse_mod7.anims.load('walk');
        mouse_mod7.anims.play('walk');
        mouse_mod7.setDepth(11);
        mouse_mod7.setScale(0.5, 0.5);

        toy_1.setDepth(7);
        toy_2.setDepth(7);
        toy_3.setDepth(7);
        toy_4.setDepth(7);
        toy_5.setDepth(7);
        toy_6.setDepth(7);
        toy_7.setDepth(7);
        toy_8.setDepth(7);
        toy_9.setDepth(7);
        toy_10.setDepth(7);
        toy_11.setDepth(7);
        toy_12.setDepth(7);
        toy_13.setDepth(7);
        toy_14.setDepth(7);
        toy_15.setDepth(7);
        toy_16.setDepth(7);
        toy_17.setDepth(7);
        toy_18.setDepth(7);
        toy_19.setDepth(7);
        toy_20.setDepth(7);
        toy_21.setDepth(7);

        this.initialTime = 60;
        camera = this.cameras.main;
        camera.setBounds(0, 0, 2500, 3000);
        this.matter.world.setBounds(0, 0, 2500, 3000).disableGravity();
        bg = this.add.image(1250, 1500, "outbg");
        bg.setDepth(0);

        if (!this.sys.game.device.os.desktop) {
            let ac = this.add.image(1800, 800, "acc");
            let rv = this.add.image(1500, 850, "rvr");
            let br = this.add.image(960, 850, "brk");
            let mr = this.add.image(500, 850, "mr");
            let ml = this.add.image(200, 850, "ml");
            ac.setDepth(100);
            rv.setDepth(100);
            br.setDepth(100);
            mr.setDepth(100);
            ml.setDepth(100);
            ac.setScrollFactor(0);
            rv.setScrollFactor(0);
            br.setScrollFactor(0);
            mr.setScrollFactor(0);
            ml.setScrollFactor(0);

            ac.setInteractive().on('pointerdown', function () {
                forward = true;
                backward = false;
            });

            ac.setInteractive().on('pointerout', function () {
                forward = false;
                backward = false;
            });

            rv.setInteractive().on('pointerdown', function () {
                backward = true;
                forward = false;
            });
            rv.setInteractive().on('pointerout', function () {
                backward = false;
                forward = false;
            });

            br.setInteractive().on('pointerdown', function () {
                brakes = true;
            });
            br.setInteractive().on('pointerout', function () {
                brakes = false;
            });

            ml.setInteractive().on('pointerdown', function () {
                left = true;
                right = false;
            });
            ml.setInteractive().on('pointerout', function () {
                left = false;
                right = false;
            });

            mr.setInteractive().on('pointerdown', function () {
                right = true;
                left = false;
            });

            mr.setInteractive().on('pointerout', function () {
                right = false;
                left = false;
            });

            forward = false;
            backward = false;
            brakes = false;
            left = false;
            right = false;
        }

        model = this.matter.add.sprite(1250, 600, "outlander");
        car_shadow = this.matter.add.image(1000, 1000, "shadow_img");
        car_shadow.body.destroy();
        l_wheel = this.matter.add.image(0, 0, "wheels");
        r_wheel = this.matter.add.image(0, 0, "wheels");
        l_wheel.displayHeight = 30;
        r_wheel.displayHeight = 30;
        r_wheel.displayWidth = 14;
        l_wheel.displayWidth = 14;
        model.setBody({
            type: 'rectangle',
            height: '260',
            width: '120'
        });

        model.setDepth(5);
        l_wheel.setDepth(4);
        r_wheel.setDepth(4);
        car_shadow.setDepth(2);
        car_shadow.setAlpha(0.5);
        l_wheel.body.destroy();
        r_wheel.body.destroy();

        camera.startFollow(model);
        cursors = this.input.keyboard.createCursorKeys();
        time = this.add.text(20, 100, '', textStyle);
        time.setScrollFactor(0);
        time.setDepth(30);
        gameTimer = this.time.addEvent({
            delay: 1000,
            callback: onEvent,
            startAt: 3000,
            callbackScope: this,
            loop: true
        });

        this.matterCollision.addOnCollideStart({
            objectA: model,
            objectB: [mouse_mod, mouse_mod2, mouse_mod3, mouse_mod4, mouse_mod5, mouse_mod6, mouse_mod7],
            callback: eventData => {
                eventData.gameObjectB.destroy();
                out_score += 50;
            }
        });

        this.matterCollision.addOnCollideStart({
            objectA: model,
            objectB: [
                toy_1,
                toy_2,
                toy_3,
                toy_4,
                toy_5,
                toy_6,
                toy_7,
                toy_8,
                toy_9,
                toy_10,
                toy_11,
                toy_12,
                toy_13,
                toy_14,
                toy_15,
                toy_16,
                toy_17,
                toy_18,
                toy_19,
                toy_20,
                toy_21,
            ],
            callback: eventData => {
                out_bump += 1;
            }
        });

        score = this.add.text(20, 20, '', textStyle);
        score.setScrollFactor(0);
        score.setDepth(20);
        bump = this.add.text(20, 60, '', textStyle);
        bump.setScrollFactor(0);
        bump.setDepth(20);
    },
    update: function () {

        if (this.initialTime === 1) {
            out_done = true;
            this.sound.stopAll();
            car_shadow.destroy();
        } else if (out_score === 350) {
            out_done = true;
            this.sound.stopAll();
            forward = false;
            backward = false;
            brakes = false;
            left = false;
            right = false;
            currentSpeed = 0;
            this.scene.start('finalScene');
        }

        if (cursors.up.isDown && currentSpeed <= 10 || forward && currentSpeed <= 10) {
            currentSpeed += 0.5;
        } else {
            if (currentSpeed > 0) {
                currentSpeed -= 0.5;
            }
        }

        if (cursors.down.isDown && currentSpeed >= -5 || backward && currentSpeed >= -5) {
            currentSpeed -= 0.5;
        } else {
            if (currentSpeed < 0) {
                currentSpeed += 0.5;
            }
        }

        if (cursors.space.isDown || brakes) {
            model.setVelocityX(currentSpeed * (Math.sin(model.rotation - model.body.angularVelocity / 0.05)));
            model.setVelocityY(currentSpeed * (-Math.cos(model.rotation - model.body.angularVelocity / 0.05)));
        } else {
            model.setVelocityX(Math.sin(model.rotation) * currentSpeed);
            model.setVelocityY(-Math.cos(model.rotation) * currentSpeed);
        }

        let leftWheelPos = {x: model.x - 42, y: model.y - 85};
        let rightWheelPos = {x: model.x + 42, y: model.y - 85};
        Phaser.Math.RotateAround(leftWheelPos, model.x, model.y, model.rotation);
        l_wheel.setPosition(leftWheelPos.x, leftWheelPos.y);
        Phaser.Math.RotateAround(rightWheelPos, model.x, model.y, model.rotation);
        r_wheel.setPosition(rightWheelPos.x, rightWheelPos.y);

        if (cursors.left.isDown || left) {
            model.setAngularVelocity(-5 * (currentSpeed / 1000));
            l_wheel.angle = model.angle - 35;
            r_wheel.angle = model.angle - 35;
        } else if (cursors.right.isDown || right) {
            model.setAngularVelocity(5 * (currentSpeed / 1000));
            l_wheel.angle = model.angle + 35;
            r_wheel.angle = model.angle + 35;
        } else {
            setTimeout(function () {
                model.setAngularVelocity(0);
            }, 100);
            l_wheel.angle = model.angle;
            r_wheel.angle = model.angle;
        }

        car_shadow.x = model.x;
        car_shadow.y = model.y;
        car_shadow.setRotation(model.rotation);

        score.setText('Очки за гостей: ' + out_score);
        bump.setText('Штраф за столкновения: ' + out_bump);
    }
});

let PajeroSportScene = Phaser.Class({
    Extends: Phaser.Scene,
    initialize:
        function L3Scene() {
            Phaser.Scene.call(this, {key: 'l3Scene'});
        },
    preload: function () {
        this.load.image("pajerosport", PajeroSport);
        this.load.image("psbg", ps_bg);
        this.load.image("wheels", wheel);
        this.load.image("shadow_img", ps_shadow);
        this.load.audio("ps_music", ps_fx);
        this.load.image("dish_1", dish_1);
        this.load.image("dish_2", dish_2);
        this.load.image("dish_3", dish_3);
        this.load.image("dish_4", dish_4);
        this.load.image("food_1", food_1);
        this.load.image("food_2", food_2);
        this.load.image("food_3", food_3);
        this.load.image("food_4", food_4);
        this.load.image("food_5", food_5);
        this.load.image("food_6", food_6);
        this.load.image("food_7", food_7);
        this.load.image("acc", acceleration);
        this.load.image("rvr", reverse);
        this.load.image("brk", brake);
        this.load.image("ml", m_left);
        this.load.image("mr", m_right);
        this.load.image("audio", audio);
    },
    create: function () {
        var ps_m = this.sound.add("ps_music");
        let playSound = this.add.image(1850, 50, 'audio').setScrollFactor(0).setDepth(200).setScale(0.1);
        let mute = false;
        playSound.setInteractive().on('pointerdown', function () {
            if (mute === false) {
                this.scene.sound.setVolume(0);
                mute = true;
            } else {
                this.scene.sound.setVolume(1);
                mute = false;
            }
        });
        let dish_1 = this.matter.add.image(1520, 1300, 'dish_1', null, {isStatic: true});
        let dish_2 = this.matter.add.image(520, 1600, 'dish_2', null, {isStatic: true});
        let dish_3 = this.matter.add.image(1320, 600, 'dish_3', null, {isStatic: true});
        let dish_4 = this.matter.add.image(320, 800, 'dish_4', null, {isStatic: true});
        let dish_5 = this.matter.add.image(1720, 2000, 'dish_1', null, {isStatic: true});
        let dish_6 = this.matter.add.image(620, 2200, 'dish_2', null, {isStatic: true});
        let dish_7 = this.matter.add.image(1800, 2500, 'dish_3', null, {isStatic: true});
        let dish_8 = this.matter.add.image(900, 2700, 'dish_4', null, {isStatic: true});
        let dish_9 = this.matter.add.image(1230, 2400, 'dish_1', null, {isStatic: true});
        let dish_10 = this.matter.add.image(9100, 2560, 'dish_2', null, {isStatic: true});

        let food_1 = this.matter.add.image(200, 400, 'food_1', null, {isStatic: true});
        let food_2 = this.matter.add.image(900, 900, 'food_2', null, {isStatic: true});
        let food_3 = this.matter.add.image(700, 1400, 'food_3', null, {isStatic: true});
        let food_4 = this.matter.add.image(1200, 1900, 'food_4', null, {isStatic: true});
        let food_5 = this.matter.add.image(1500, 2400, 'food_5', null, {isStatic: true});
        let food_6 = this.matter.add.image(2000, 2700, 'food_6', null, {isStatic: true});
        let food_7 = this.matter.add.image(1800, 800, 'food_7', null, {isStatic: true});
        let food_8 = this.matter.add.image(2100, 1400, 'food_1', null, {isStatic: true});
        let food_9 = this.matter.add.image(1800, 1800, 'food_3', null, {isStatic: true});
        let food_10 = this.matter.add.image(200, 1300, 'food_5', null, {isStatic: true});
        let food_11 = this.matter.add.image(2300, 2800, 'food_7', null, {isStatic: true});
        food_1.setDepth(11);
        food_2.setDepth(11);
        food_3.setDepth(11);
        food_4.setDepth(11);
        food_5.setDepth(11);
        food_6.setDepth(11);
        food_7.setDepth(11);
        food_8.setDepth(11);
        food_9.setDepth(11);
        food_10.setDepth(11);
        food_11.setDepth(11);
        food_1.setScale(0.5, 0.5);
        food_2.setScale(0.5, 0.5);
        food_3.setScale(0.5, 0.5);
        food_4.setScale(0.5, 0.5);
        food_5.setScale(0.5, 0.5);
        food_6.setScale(0.5, 0.5);
        food_7.setScale(0.5, 0.5);
        food_8.setScale(0.5, 0.5);
        food_9.setScale(0.5, 0.5);
        food_10.setScale(0.5, 0.5);
        food_11.setScale(0.5, 0.5);
        dish_1.setScale(0.6, 0.6);
        dish_2.setScale(0.5, 0.5);
        dish_3.setScale(0.9, 0.9);
        dish_4.setScale(0.5, 0.5);
        dish_5.setScale(0.4, 0.4);
        dish_6.setScale(0.7, 0.7);
        dish_7.setScale(0.6, 0.6);
        dish_8.setScale(0.7, 0.7);
        dish_9.setScale(0.5, 0.5);
        dish_10.setScale(0.7, 0.7);
        dish_1.setDepth(10);
        dish_2.setDepth(10);
        dish_3.setDepth(10);
        dish_4.setDepth(10);
        dish_5.setDepth(10);
        dish_6.setDepth(10);
        dish_7.setDepth(10);
        dish_8.setDepth(10);
        dish_9.setDepth(10);
        dish_10.setDepth(10);
        ps_m.play({loop: true});
        this.initialTime = 60;
        camera = this.cameras.main;
        camera.setBounds(0, 0, 2500, 3000);
        this.matter.world.setBounds(0, 0, 2500, 3000).disableGravity();
        bg = this.add.image(1250, 1500, "psbg");
        bg.setDepth(0);
        bg.setAngle(90);

        if (!this.sys.game.device.os.desktop) {
            let ac = this.add.image(1800, 800, "acc");
            let rv = this.add.image(1500, 850, "rvr");
            let br = this.add.image(960, 850, "brk");
            let mr = this.add.image(500, 850, "mr");
            let ml = this.add.image(200, 850, "ml");
            ac.setDepth(100);
            rv.setDepth(100);
            br.setDepth(100);
            mr.setDepth(100);
            ml.setDepth(100);
            ac.setScrollFactor(0);
            rv.setScrollFactor(0);
            br.setScrollFactor(0);
            mr.setScrollFactor(0);
            ml.setScrollFactor(0);

            ac.setInteractive().on('pointerdown', function () {
                forward = true;
                backward = false;
            });

            ac.setInteractive().on('pointerout', function () {
                forward = false;
                backward = false;
            });

            rv.setInteractive().on('pointerdown', function () {
                backward = true;
                forward = false;
            });
            rv.setInteractive().on('pointerout', function () {
                backward = false;
                forward = false;
            });

            br.setInteractive().on('pointerdown', function () {
                brakes = true;
            });
            br.setInteractive().on('pointerout', function () {
                brakes = false;
            });

            ml.setInteractive().on('pointerdown', function () {
                left = true;
                right = false;
            });
            ml.setInteractive().on('pointerout', function () {
                left = false;
                right = false;
            });

            mr.setInteractive().on('pointerdown', function () {
                right = true;
                left = false;
            });

            mr.setInteractive().on('pointerout', function () {
                right = false;
                left = false;
            });

            forward = false;
            backward = false;
            brakes = false;
            left = false;
            right = false;
        }

        model = this.matter.add.sprite(500, 500, "pajerosport");
        car_shadow = this.matter.add.image(1000, 1000, "shadow_img");
        car_shadow.body.destroy();
        l_wheel = this.matter.add.image(0, 0, "wheels");
        r_wheel = this.matter.add.image(0, 0, "wheels");
        l_wheel.displayHeight = 36;
        r_wheel.displayHeight = 36;
        r_wheel.displayWidth = 14;
        l_wheel.displayWidth = 14;

        model.setBody({
            type: 'rectangle',
            height: '280',
            width: '90'
        });

        model.setDepth(5);
        l_wheel.setDepth(4);
        r_wheel.setDepth(4);
        car_shadow.setDepth(2);
        car_shadow.setAlpha(0.5);
        l_wheel.body.destroy();
        r_wheel.body.destroy();

        //Outlander_level/Out_toys's init sets
        camera.startFollow(model);
        cursors = this.input.keyboard.createCursorKeys();
        time = this.add.text(20, 100, '', textStyle);
        time.setScrollFactor(0);
        time.setDepth(30);
        gameTimer = this.time.addEvent({
            delay: 1000,
            callback: onEvent,
            startAt: 3000,
            callbackScope: this,
            loop: true
        });

        this.matterCollision.addOnCollideStart({
            objectA: model,
            objectB: [food_1, food_2, food_3, food_4, food_5, food_6, food_7, food_8, food_9, food_10, food_11],
            callback: eventData => {
                eventData.gameObjectB.destroy();
                ps_score += 10;
            }
        });

        this.matterCollision.addOnCollideStart({
            objectA: model,
            objectB: [dish_1, dish_2, dish_3, dish_4, dish_5, dish_6, dish_7, dish_8, dish_9, dish_10],
            callback: eventData => {
                ps_bump += 1;
            }
        });

        score = this.add.text(20, 20, '', textStyle);
        score.setScrollFactor(0);
        score.setDepth(20);
        bump = this.add.text(20, 60, '', textStyle);
        bump.setScrollFactor(0);
        bump.setDepth(20);
    },
    update: function () {

        if (this.initialTime === 1) {
            ps_done = true;
            car_shadow.destroy();
            this.sound.stopAll();
        } else if (ps_score === 110) {
            ps_done = true;
            this.sound.stopAll();
            forward = false;
            backward = false;
            brakes = false;
            left = false;
            right = false;
            currentSpeed = 0;
            this.scene.start('finalScene');
        }

        if (cursors.up.isDown && currentSpeed <= 10 || forward && currentSpeed <= 10) {
            currentSpeed += 0.5;
        } else {
            if (currentSpeed > 0) {
                currentSpeed -= 0.5;
            }
        }

        if (cursors.down.isDown && currentSpeed >= -5 || backward && currentSpeed >= -5) {
            currentSpeed -= 0.5;
        } else {
            if (currentSpeed < 0) {
                currentSpeed += 0.5;
            }
        }

        if (cursors.space.isDown || brakes) {
            model.setVelocityX(currentSpeed * (Math.sin(model.rotation - model.body.angularVelocity / 0.05)));
            model.setVelocityY(currentSpeed * (-Math.cos(model.rotation - model.body.angularVelocity / 0.05)));
        } else {
            model.setVelocityX(Math.sin(model.rotation) * currentSpeed);
            model.setVelocityY(-Math.cos(model.rotation) * currentSpeed);
        }

        let leftWheelPos = {x: model.x - 45, y: model.y - 90};
        let rightWheelPos = {x: model.x + 45, y: model.y - 90};
        Phaser.Math.RotateAround(leftWheelPos, model.x, model.y, model.rotation);
        l_wheel.setPosition(leftWheelPos.x, leftWheelPos.y);
        Phaser.Math.RotateAround(rightWheelPos, model.x, model.y, model.rotation);
        r_wheel.setPosition(rightWheelPos.x, rightWheelPos.y);

        if (cursors.left.isDown || left) {
            model.setAngularVelocity(-5 * (currentSpeed / 1000));
            l_wheel.angle = model.angle - 35;
            r_wheel.angle = model.angle - 35;
        } else if (cursors.right.isDown || right) {
            model.setAngularVelocity(5 * (currentSpeed / 1000));
            l_wheel.angle = model.angle + 35;
            r_wheel.angle = model.angle + 35;
        } else {
            setTimeout(function () {
                model.setAngularVelocity(0);
            }, 100);
            l_wheel.angle = model.angle;
            r_wheel.angle = model.angle;
        }

        car_shadow.x = model.x;
        car_shadow.y = model.y;
        car_shadow.setRotation(model.rotation);

        score.setText('Очки за продукты: ' + ps_score);
        bump.setText('Штраф за столкновения: ' + ps_bump);
    }
});

//help functions
function onEvent() {
    this.initialTime -= 1;
    time.setText('Осталось времени: ' + formatTime(this.initialTime));
    if (this.initialTime === 0) {
        this.scene.start('finalScene');
    }
}

function onBootL200() {
    this.initialTime -= 1;
    if (this.initialTime === 0) {
        this.scene.start('l1Scene');
    }
}

function onBootPS() {
    this.initialTime -= 1;
    if (this.initialTime === 0) {
        this.scene.start('l2Scene');
    }
}

function onBootOut() {
    this.initialTime -= 1;
    if (this.initialTime === 0) {
        this.scene.start('l3Scene');
    }
}

function onBootLoad() {
    this.initialTime -= 1;
    if (this.initialTime === 0) {
        this.scene.start('loaderScene');
    }
}

function formatTime(seconds) {
    let minutes = Math.floor(seconds / 60);
    let secondsParts = seconds % 60;
    secondsParts = secondsParts.toString().padStart(2, '0');
    return `${minutes}:${secondsParts}`;
}

function total(total, score, bump) {
    total = score - bump;
    return total;
}

function countScore(currentScore, type) {
    switch (type) {
        case 'l200':
            currentScore.setText('Mitsubishi L200 - всего очков: ' + total(l200_total, l200_score, l200_bump));
            break;
        case 'outlander':
            currentScore.setText('Mitsubishi Outlander - всего очков: ' + total(out_total, out_score, out_bump));
            break;
        case 'ps':
            currentScore.setText('Mitsubishi Pajero Sport - всего очков: ' + total(ps_total, ps_score, ps_bump));
            break;
    }
}

function bootOrder(type, info, info2) {
    switch (type) {
        case 'l1Scene':
            info.setText('Привезите на праздник ёлку и игрушки используя l200. У тебя есть 1 минута!\n');
            break;
        case 'l2Scene':
            info.setText('С помощью 7-ми местного Outlander доставь членов крысиной семьи к праздничному столу!');
            info2.setText('Торопись время ограничено.');
            break;
        case 'l3Scene':
            info2.setText('Какой новый год без оливье?');
            info.setText('Собери за 1 минуту все ингридиенты с помощью Pajero Sport и привези к столу!');
            break;
    }
}

function audioControl(playSound) {
    let mute = false;
    playSound.setInteractive().on('pointerdown', function () {
        if (mute === false) {
            this.scene.sound.setVolume(0);
            mute = true;
        } else {
            this.scene.sound.setVolume(1);
            mute = false;
        }
    });
}

const game_config = {
    type: Phaser.CANVAS,
    width: 1920,
    height: 1080,
    physics: {
        default: 'matter',
        matter: {
            gravity: {
                x: false,
                y: false
            }
        }
    },
    plugins: {
        scene: [{
            plugin: PhaserMatterCollisionPlugin, // The plugin class
            key: "matterCollision", // Where to store in Scene.Systems, e.g. scene.sys.matterCollision
            mapping: "matterCollision" // Where to store in the Scene, e.g. scene.matterCollision
        }],
    },
    input: {
        activePointers: 5,
    },
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        orientation: Phaser.Scale.Orientation.PORTRAIT,
    },
    autoRound: false,
    banner: {
        hidePhaser: true
    },
    scene: [LoaderScene, L200Scene, OutlanderScene, PajeroSportScene, FinalScene, BootScene]
};

const game = new Phaser.Game(game_config);